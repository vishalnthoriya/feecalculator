<?php

use app\models\ImportCsv;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $importform ImportCsv */


?>

<div class="site-index">

        <?php $form = ActiveForm::begin(['method'=>'post']); ?>
        <?= $form->field($importform, 'csv')->fileInput(); ?>
        <?= Html::submitButton('Import', ['class' => 'btn btn-primary', 'name' => 'submit']) ?>
        <?php ActiveForm::end(); ?>
    
        <?php if(Yii::$app->request->post() && !empty($error_rows)) {   
                
                foreach($error_rows as $item) 
                {
                    echo ' FAILE TO IMPORT: '.$item[0].'->'.$item[1].'<br>';
                }
            
            
        } 
          ?>
</div>


