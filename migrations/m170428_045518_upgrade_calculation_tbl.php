<?php

use yii\db\Migration;

class m170428_045518_upgrade_calculation_tbl extends Migration
{
    public function up()
    {
        // ADD NEW COLUMN
        
        $this->addColumn('calculatoins','grade_requirment', \yii\db\Schema::TYPE_STRING);
        $this->addColumn('calculatoins','grade_source', ' VARCHAR(2000)');
        $this->addColumn('calculatoins','tution_fee_source_requirment', ' VARCHAR(2000) ');
    }

    public function down()
    {
        echo "m170428_045518_upgrade_calculation_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
