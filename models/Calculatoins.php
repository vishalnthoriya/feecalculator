<?php
namespace app\models;

use app\models\Programs;
use app\models\Universities;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;



/**
 * This is the model class for table "calculatoins".
 *
 * @property string $id
 * @property string $university_id
 * @property string $program_id
 * @property double $tution_international
 * @property double $tution_canada
 * @property double $tution_province
 * @property double $total_fees
 * @property double $residence_fees
 * @property double $meal_plan
 * @property string $grade_requirment 
 * @property string $grade_source
 * @property string $tution_fee_source_requirment	
 * @property Programs $program
 * @property Universities $university
 */
class Calculatoins extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calculatoins';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['university_id', 'program_id', 'tution_international', 'tution_canada', 'tution_province', 'total_fees', 'residence_fees', 'meal_plan'], 'required'],
            [['university_id', 'program_id'], 'integer'],
            [['grade_requirment','grade_source','tution_fee_source_requirment'],'safe'],
            [['tution_international', 'tution_canada', 'tution_province', 'total_fees', 'residence_fees', 'meal_plan'], 'number'],
            [['program_id'], 'exist', 'skipOnError' => true, 'targetClass' => Programs::className(), 'targetAttribute' => ['program_id' => 'id']],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => Universities::className(), 'targetAttribute' => ['university_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'university_id' => 'University ID',
            'program_id' => 'Program ID',
            'tution_international' => 'Tution International',
            'tution_canada' => 'Tution Canada',
            'tution_province' => 'Tution Province',
            'total_fees' => 'Total Fees',
            'residence_fees' => 'Residence Fees',
            'meal_plan' => 'Meal Plan',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProgram()
    {
        return $this->hasOne(Programs::className(), ['id' => 'program_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUniversity()
    {
        return $this->hasOne(Universities::className(), ['id' => 'university_id']);
    }
}
