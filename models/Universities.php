<?php
namespace app\models;

use app\models\Calculatoins;
use app\models\State;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "universities".
 *
 * @property string $id
 * @property string $name
 * @property string $province_id
 *
 * @property Calculatoins[] $calculatoins
 * @property State $province
 */
class Universities extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'universities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['province_id'], 'integer'],
            [['name'], 'string', 'max' => 2000],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'province_id' => 'Province ID',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCalculatoins()
    {
        return $this->hasMany(Calculatoins::className(), ['university_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProvince()
    {
        return $this->hasOne(State::className(), ['id' => 'province_id']);
    }
}
