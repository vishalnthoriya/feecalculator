<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *  
 *
 *  
 *
 */
class ImportCsv extends Model
{
    public $csv;
    


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['csv'], 'file'],
            ['state','safe']
            // rememberMe must be a boolean value
             
        ];
    }
    public function upload()
    {
         
            $file_name =  $this->csv->baseName .time(). '.' . $this->csv->extension;
            $file_name = preg_replace("/[^a-zA-Z0-9.]/", "", $file_name);
            $dir = 'uploads/';
            if(!file_exists($dir))
                mkdir($dir);
            $response = $this->csv->saveAs($dir . $file_name );
            if($response == true)
                return [$file_name,$dir.$file_name];
            else 
                return false;
         
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
         return [
             'csv'=>'Please Upload Proper csv',
             'program'=>'Select Program',
             'country'=>'Country',
             'state'=>'Province'
         ];
    }
    

     

     
}
